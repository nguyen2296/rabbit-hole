import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:wikiapp/helpers/getUrlRandom.dart';
import 'PlatformTabBar.dart';
import 'PlatformWidget.dart';

class PlatformScaffoldWidget
    extends PlatformWidget<CupertinoTabScaffold, Scaffold> {
  final Color color;
  final Widget child;
  final PlatformTabBar bottomNavigationBar;
  final String appBartitle;

  PlatformScaffoldWidget({
    this.color,
    this.child,
    this.bottomNavigationBar,
    this.appBartitle,
  });

  @override
  CupertinoTabScaffold createIosWidget(BuildContext context) =>
       CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: bottomNavigationBar.tabItems,
          onTap: bottomNavigationBar.onTap,
          currentIndex: bottomNavigationBar.currentIndex,
          activeColor: Colors.blueAccent[300],
        ),
        backgroundColor: color,
        tabBuilder: (context, index) {
          return child;
        },
      );

  @override
  Scaffold createAndroidWidget(BuildContext context) =>  Scaffold(
        appBar: AppBar(
          title:  Center(
            child:  Text(
              appBartitle ?? 'Title',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        backgroundColor: color,
        body: child,
        endDrawer: Drawer(
          child: GridView.count(
            crossAxisCount: 2,
            children: <Widget>[
              TabBox(
                onTap: () {Navigator.pushReplacementNamed(context, '/graph', arguments: null);},
                title: 'Graph',
                icon: CupertinoIcons.eye_solid,
              ),
              TabBox(
                onTap: () {Navigator.pushReplacementNamed(context, '/read', arguments: getUrlRandom());},
                title: 'Read',
                icon: CupertinoIcons.news_solid,
              ),
              TabBox(
                onTap: () {Navigator.pushReplacementNamed(context, '/history');},
                title: 'History',
                icon: CupertinoIcons.time_solid,
              ),
              TabBox(
                onTap: () {Navigator.pushReplacementNamed(context, '/notebook');},
                title: 'Notebook',
                icon: CupertinoIcons.bookmark_solid,
              ),
          ],),
        ),
      );
}

class TabBox extends StatelessWidget {
  final IconData icon;
  final String title;
  final onTap;
  
  TabBox({
    @required this.icon, 
    @required this.title, 
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius:  BorderRadius.all(Radius.circular(8.0)),
          boxShadow: const [
            BoxShadow(
              blurRadius: 10,
              color: const Color(0xFFE3F2FD),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(icon, size: 50, color: Colors.pinkAccent,),
            SizedBox(height: 8.0,),
            Text(title, style: TextStyle(fontSize: 15, color: Colors.indigo, fontWeight: FontWeight.bold),)
          ],),
      ),
    );
  }
}