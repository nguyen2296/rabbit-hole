int getMaxSession(Map<dynamic,dynamic> map) {
  int result = 0;
  map.forEach((key, value){
    if (value > result) {
      result = value;
    }
  });
  return result;
}
