import 'package:wikiapp/ReduxStore.dart';
import 'package:wikiapp/modules/read/redux/models.dart';

getLastSession() {
    ReadSession lastSession;
    try {
      lastSession = store.state.readState.sessions.last;
    } catch (e) {
      lastSession = null;
    }
    return lastSession;
}
