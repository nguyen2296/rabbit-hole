import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:wikiapp/ReduxIStore.dart';
import 'package:wikiapp/components/PlatformButton.dart';
import 'package:wikiapp/components/NoteDialog.dart';
import 'package:wikiapp/components/PlatformScaffoldWidget.dart';
import 'package:wikiapp/components/PlatformTextField.dart';
import 'package:wikiapp/components/PlatformTabBar.dart';
import 'package:wikiapp/modules/notebook/redux/action.dart';
import 'package:wikiapp/helpers/getUrlRandom.dart';
import 'package:wikiapp/modules/notebook/redux/models.dart';
import 'package:wikiapp/routers.dart';
import 'package:reorderables/reorderables.dart';

class _ViewModel {
  final notebookState;
  final updateNoteBook;
  final deleteNoteBook;
  final reorderNoteBook;
  _ViewModel({
    @required this.notebookState,
    @required this.updateNoteBook,
    @required this.deleteNoteBook,
    @required this.reorderNoteBook,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _ViewModel &&
          runtimeType == other.runtimeType &&
          notebookState == other.notebookState;

  @override
  int get hashCode => notebookState.hashCode;
}

class NotebookPage extends StatefulWidget {
  @override
  _NotebookState createState() => _NotebookState();
}

class _NotebookState extends State<NotebookPage> {
  @override
  void initState() {
    super.initState();
  }

  Widget bottomNavigationBar() {
    return PlatformTabBar(
      onTap: (index) {
        if (index == 3) {
          Navigator.pushReplacementNamed(context, routes[index], arguments: getUrlRandom());
          return ;
        }
        if (index != 1) {
          Navigator.pushReplacementNamed(context, routes[index]);
        }
      },
      currentIndex: 1,
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      onInit: (store) {},
      distinct: true,
      converter: (store) {
        return  _ViewModel(
          deleteNoteBook: (index) => store.dispatch(DeleteNotebook(index)),
          reorderNoteBook: (oldIndex, newIndex) => store.dispatch(ReOrderNotebook(oldIndex, newIndex)),
          notebookState: store.state.notebookState,
          updateNoteBook: (index, newNote) => store.dispatch(UpdateNotebook(index, newNote)),
        );
      },
      builder: (context, viewModel) {
        final List<Note> notes = viewModel.notebookState.notes;

        return PlatformScaffoldWidget(
          appBartitle: "Notebook",
          bottomNavigationBar: bottomNavigationBar(),
          child: SingleChildScrollView(child: SafeArea(child: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              child: ReorderableColumn(
                scrollController: ScrollController(),
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(notes.length, (index) {
                  final Note eachNote = notes[index];
                  return Container(
                    padding:  EdgeInsets.only(left: 5.0, right: 10.0, top: 15.0, bottom: 15.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:  BorderRadius.all(Radius.circular(8.0)),
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 20,
                          color: const Color(0xFFE3F2FD),
                        ),
                      ],
                    ),
                    key: ValueKey("value$index"),
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: NoteItem(
                      eachNote: eachNote,
                      index: index,
                      viewModel: viewModel,
                    ),
                  );
                }),
                onReorder: (int oldIndex, int newIndex) {
                  viewModel.reorderNoteBook(oldIndex, newIndex);
                },
              ),
            ),
          ],),
        )));
      },
    );
  }
}

class EditingButton extends StatefulWidget {
  final Note note;
  final _ViewModel viewModel;
  final int index;

  EditingButton(
      {Key key,
      @required this.note,
      @required this.viewModel,
      @required this.index})
      : super(key: key);

  @override
  _EditingButtonState createState() => _EditingButtonState();
}

class _EditingButtonState extends State<EditingButton> {
  TextEditingController _noteController;

  @override
  void initState() {
    super.initState();
    _noteController = TextEditingController(text: widget.note.note);
  }

  _showEditingDialog(context) {
    List actions = <Widget>[
      PlatformButton(
        child: Text('Cancel'),
        onPressed: () { Navigator.of(context).pop(); },
      ),
      PlatformButton(
        child: Text('Save'),
        onPressed: () {
          widget.viewModel
              .updateNoteBook(widget.index, _noteController.text);
          Navigator.of(context).pop();
        },
      )
    ];

    showDialog(
        context: context,
        builder: (context) {
          return NoteDialog(
            textField: Column(children: <Widget>[
              Align(
                child: Text( 'Note', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                alignment: Alignment.bottomLeft,
              ),
              Padding(
                  child: PlatformTextField(controller: _noteController),
                  padding: EdgeInsets.only( bottom: 20.0)),
              Align(
                child: Text('URL', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                alignment: Alignment.bottomLeft,
              ),
              Text(widget.note.url, textAlign: TextAlign.start)
            ]),
            actions: actions,
          );
        });
  }

  onChangeOption(newValue, context) {
    
    if (newValue == 'Delete') {
      return widget.viewModel.deleteNoteBook(widget.index);
    }

    if (newValue == 'Edit') {
      return _showEditingDialog(context);
    }

    if (newValue == 'Open') {
      return Navigator.pushReplacementNamed(context, '/read', arguments: widget.note.url);
    }
  }

  @override
  Widget build(BuildContext context) {
    return  DropdownButton<String>(
      underline: Container(
        height: 0.0,
        width: 0.0,
      ),
      icon: Icon(Icons.more, color: Colors.indigoAccent),
      iconSize: 24,
      onChanged: (String newValue) {
        onChangeOption(newValue, context);
      },
      style: TextStyle(color: Colors.indigoAccent, fontSize: 14),
      items: [
        {'label': 'Open', 'icon': Icons.open_in_new}, 
        {'label': 'Edit', 'icon': Icons.edit},
        {'label': 'Delete', 'icon': Icons.delete}
      ].map((value) {
        return DropdownMenuItem<String>(
          value: value['label'],
          child: Row(
              children: <Widget>[
                Icon(value['icon'], size: 16.0, color: Colors.indigoAccent,),
                Container(child: Text( value['label']), margin: EdgeInsets.only(left: 10.0),)
              ],
            ),
        );
      }).toList(),
    );
  }
}

class NoteItem extends StatelessWidget {
  final Note eachNote;
  final _ViewModel viewModel;
  final int index;

  NoteItem({
    @required this.eachNote, 
    @required this.index, 
    @required this.viewModel,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.bookmark,
          color: Colors.pinkAccent,
          size: 40,
        ),
        SizedBox(width: 10.0,),
        Flexible(child: 
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Align(
            alignment: Alignment.bottomLeft, 
            child: Padding(
              padding: EdgeInsets.only(bottom: 5.0),
              child:  Text(
              'Note: ${eachNote.note}', 
              style: TextStyle(color: Colors.indigo, 
              fontWeight: FontWeight.bold,
              fontSize: 16
              )))),
          Align(
            alignment: Alignment.bottomLeft, 
            child:  Text(
              'URL: ${eachNote.url}', 
              style: TextStyle(color: Colors.indigo[400], 
              fontSize: 12.0
            ))),
        ]),),
        Material(child:EditingButton(note: eachNote, viewModel: viewModel, index: index))
      ],
    );
  }
}
