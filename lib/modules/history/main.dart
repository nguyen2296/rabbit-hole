import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:wikiapp/ReduxIStore.dart';
import 'package:wikiapp/ReduxStore.dart';
import 'package:wikiapp/components/PlatformScaffoldWidget.dart';
import 'package:wikiapp/components/PlatformTabBar.dart';
import 'package:wikiapp/helpers/getUrlRandom.dart';
import 'package:wikiapp/modules/read/redux/models.dart';
import 'package:wikiapp/routers.dart';
import 'package:duration/duration.dart';

class _ViewModel {
  final readState;

  _ViewModel(
      {@required this.readState});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _ViewModel &&
          runtimeType == other.runtimeType &&
          readState == other.readState;

  @override
  int get hashCode => readState.hashCode;
}

class HistoryPage extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<HistoryPage> {
  List<Widget> listItems;

  Widget getLeading() {
    return Icon(
      CupertinoIcons.time_solid,
      color: Colors.pinkAccent,
      size: 35,
    );
  }

  Widget getTitle(session) {
    return Text(
      DateFormat.jm()
          .add_yMMMd()
          .format(session.endAt.toLocal())
          .toString(),
      overflow: TextOverflow.ellipsis,
      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.indigo),
    );
  }

  Widget getTrailing(totalUrls) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          totalUrls.toString(),
          style: TextStyle(fontSize: 16, color: Colors.indigo[400], fontWeight: FontWeight.bold),
        ),
        Icon(
          CupertinoIcons.news_solid,
          color: Colors.indigoAccent,
        ),
      ],
    );
  }

  List<Widget> getListItems(List<ReadSession> sessions) {
    List<Widget> res = [];
    for (var session in sessions) {
      var totalTime = 0;
      session.readSessions.forEach((k, v) => totalTime += v);
      var totalUrls = session.readSessions.length;
      var totalTimeString =
        printDuration(Duration(seconds: totalTime), abbreviated: true);
      var item = Column(
        children: <Widget>[ListTile(
          leading: getLeading(),
          title: getTitle(session),
          subtitle: Text(totalTimeString.toString(), style: TextStyle(color: Colors.indigo[400]),),
          trailing: getTrailing(totalUrls),
          onTap: () { Navigator.pushReplacementNamed(context, '/graph', arguments: session); },
        ),
          Divider(color: Colors.indigoAccent, thickness: 1.0, indent: 70.0, endIndent: 20.0,)
        ]
        );
      res.add(item);
    }
    return res;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      listItems = getListItems(store.state.readState.sessions);
    });
  }

  Widget getEmptyHistory(context, viewModel) {
    return PlatformScaffoldWidget(
      appBartitle: "History",
      bottomNavigationBar: PlatformTabBar(
        onTap: (index) {
          if (index == 3) {
            Navigator.pushReplacementNamed(context, routes[index], arguments: getUrlRandom());
            return;
          }
          if (index != 2) {
            Navigator.pushReplacementNamed(context, routes[index]);
          }
        },
        currentIndex: 2,
      ),
      child: Center(
        child: Text('empty sessions'),
      ),
    );
  }

  Widget bottomNavigationBar() {
    return PlatformTabBar(
      onTap: (index) {
        if (index == 3) {
          Navigator.pushReplacementNamed(context, routes[index], arguments: getUrlRandom());
          return;
        }
        if (index != 2) {
          Navigator.pushReplacementNamed(context, routes[index]);
        }
      },
      currentIndex: 2,
    );
  }

  Widget getHistory(context, viewModel) {
    return PlatformScaffoldWidget(
      appBartitle: "History",
      bottomNavigationBar: bottomNavigationBar(),
      child: SafeArea(
        child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius:  BorderRadius.all(Radius.circular(8.0)),
            boxShadow: const [
              BoxShadow(
                blurRadius: 20,
                color: const Color(0xFFE3F2FD),
              ),
            ],
          ),
          child: Material(
            child: ListView(
              children: listItems,
              shrinkWrap: true,
            ),
          ),
        )
      ])));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      onInit: (store) {},
      distinct: true,
      converter: (store) {
        return _ViewModel(
          readState: store.state.readState);
      },
      builder: (context, viewModel) {
        return listItems.length == 0
          ? getEmptyHistory(context, viewModel)
          : getHistory(context, viewModel);
      },
    );
  }
}
