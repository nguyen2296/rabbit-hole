# Rabbit-hole app
- This app has 4 pages including Graph, Read, History and Notebook page.
- The first page of the app is Graph. We have a line chart describing the total of time that user reads an article for each session.
- The history page helps user know that how many sessions user has and another information like the total of time, articles for each session.
- The notenook page shows a list of notes and user can edit, open and delete the note.
- The read page shows a wikipedia article and user can make a note if they need.

## Release

- IOS

```
flutter build ios
```

- Android for each target ABI

```
flutter build apk --split-per-abi
```

- Android for all target ABIs

```
flutter build apk
```

## Installation & Starting

- Run to install any package

```
flutter pub get
flutter pub run flutter_launcher_icons:main
```

- Run with debug mode

```
flutter run
```

- Run with release mode

```
flutter run --release
```
